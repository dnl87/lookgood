<?php
  session_start();
  
  require_once 'connect.php';
  require_once 'objects\Item.php';
  require_once 'objects\Clothing.php';
  require_once 'objects\User.php';
  
  $shirt = filter_input( INPUT_POST, 'shirt' );
  $trui = filter_input( INPUT_POST, 'trui' );
  $broek = filter_input( INPUT_POST, 'broek' );
  $jas = filter_input( INPUT_POST, 'jas' );
  $hoofddeksel = filter_input( INPUT_POST, 'hoofddeksel' );
  $overhemd = filter_input( INPUT_POST, 'overhemd' );
  $schoenen = filter_input( INPUT_POST, 'schoenen' );
  
  $properties = [];
  
  //get type
  switch ( isset( $shirt ) ) {
    case isset( $shirt ):
      $properties['type'] = "shirt";
      break;
    case isset( $trui ):
      $properties['type'] = "trui";
      break;
    case isset( $broek ):
      $properties['type'] = "broek";
      break;
    case isset( $jas ):
      $properties['type'] = "jas";
      break;
    case isset( $hoofddeksel ):
      $properties['type'] = "hoofddeksel";
      break;
    case isset( $overhemd ):
      $properties['type'] = "overhemd";
      break;
    case isset( $schoenen ):
      $properties['type'] = "schoenen";
      break;
  }
  
  $addMore = filter_input( INPUT_POST, 'addMore' );
  $generate = filter_input( INPUT_POST, 'generate' );
  
  $properties['season'] = filter_input( INPUT_POST, 'season' );
  $properties['brand'] = filter_input( INPUT_POST, 'brand' );
  $properties['color'] = filter_input( INPUT_POST, 'color' );
  $properties['subtype'] = filter_input( INPUT_POST, 'subtype' );
  $properties['material'] = filter_input( INPUT_POST, 'material' );
  $properties['dateOfPurchase'] = filter_input( INPUT_POST, 'dateOfPurchase' );
  $properties['description'] = filter_input( INPUT_POST, 'description' );
  $properties['available'] = filter_input( INPUT_POST, 'available' );
  $properties['price'] = filter_input( INPUT_POST, 'price' );
  $properties['size'] = filter_input( INPUT_POST, 'size' );
  
  $style = filter_input( INPUT_POST, 'style' );
  
  switch( $style ) {
    case "Arty":
      $properties['style'] = "Arty";
      break;
    case "Chic":
      $properties['style'] = "Chic";
      break;
    case "Classic":
      $properties['style'] = "Classic";
      break;
    case "Casual":
      $properties['style'] = "Casual";
      break;
    case "Exotic":
      $properties['style'] = "Exotic";
      break;
    case "Sophisticated":
      $properties['style'] = "Sophisticated";
      break;
    case "Western":
      $properties['style'] = "Western";
      break;
    case "Traditional":
      $properties['style'] = "Traditional";
      break;
    case "Punk":
      $properties['style'] = "Punk";
      break;
    case "Rocker":
      $properties['style'] = "Rocker";
      break;
    case "Gothic":
      $properties['style'] = "Gothic";
      break;
  }
  
  $item = new must\Clothing( $conn );
  
  $properties['userId'] = $_SESSION["userId"];
  
  $set = $item->insertClothing( $properties );
  $_SESSION["itemAdded"] = true;
  
  if( isset( $addMore ) && $set ) {
    header( "location: addItem.php" );
  }
  if( isset( $generate ) && $set ) {
    header( "location: preference.php" );
  }
  else {
    $_SESSION["itemAdded"] = false;
    header( "location: addItem.php" );
  }