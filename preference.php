<?php
  session_start();
  $userId = $_SESSION['userId'];
  
  require_once 'connect.php';
  require_once 'objects\Item.php';
  require_once 'objects\Clothing.php';
  require_once 'objects\User.php';
  
  $clothing = new must\Clothing( $conn );
?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="static/css/general.css" />
    <link rel="stylesheet" type="text/css" href="static/css/preference.css" />
    
    <script src="static/libs/jquery-2.1.4.js" ></script>
    
    <link rel='stylesheet' href='static/libs/spectrum/spectrum.css' />
    <script src='static/libs/spectrum/spectrum.js'></script>
    
    <title>Preferences</title>
  </head>
  <body>
    <div id="mainContainer">
      <div id='containerCenter' style="position: relative">
        <img src="static/img/beeldmerk_MuStLG.png" class="logo" />
        <a href="login.php" class="logout btn">Log uit</a>
        
        <h1>Geef je voorkeur aan</h1>
        <h6><span class="requiredStar">*</span> verplicht</h6>
        <form method="post" action="match.php">
          <div class="inlineBlock">
            <div class="selectDiv">
              <div class="selectLabel">Merk</div>
              <div class="select">
                <select name="brand">
      <?php
                  echo "<option></option>";
                  $options = $clothing->createSelectOptions( $userId, "brand" );
                  foreach( $options as $key => $value ) {
                    echo $value;
                  }
      ?>
                </select>
              </div>
            </div>

            <div class="selectDiv">
              <div class="selectLabel">Stijl *</div>
              <div class="select">
                <select name="style">
      <?php
                  $sql = "SELECT style FROM item INNER JOIN clothing ON item.item_ID = clothing.item_ID WHERE user_ID = '$userId' GROUP BY style";

                  $result = $conn->query( $sql );

                  // kijken of er resultaten zijn
                  if ( $result->num_rows > 0 ) {
                    while( $row = $result->fetch_assoc() ) {
                      $style = $row['style'];
                      echo '<option value="'.$style.'">'.ucfirst( strtolower( $style ) ).'</option>';
                    }
                  }
      ?>
                </select>
              </div>
            </div>

            <div class="selectDiv">
              <div class="selectLabel">Seizoen *</div>
              <div class="select">
                <select name="season">
      <?php
                  $sql = "SELECT season FROM item INNER JOIN clothing ON item.item_ID = clothing.item_ID WHERE user_ID = '$userId' GROUP BY season";

                  $result = $conn->query( $sql );

                  // kijken of er resultaten zijn
                  if ( $result->num_rows > 0 ) {
                    while( $row = $result->fetch_assoc() ) {
                      $season = $row['season'];
                      echo '<option value="'.$season.'">'.ucfirst( strtolower( $season ) ).'</option>';
                    }
                  }
      ?>
                </select>
              </div>
            </div>
          </div>
          <div style="clear:both;"></div>

          <script>
            var selected = false;
            function select(item) {
              if($('.'+item+'').attr('src') === 'static/img/icon_'+item+'.png') {
                $('.'+item+'').attr('src', 'static/img/icon_'+item+'_selected.png');
                $('.'+item+'Input').prop('checked', true);
                selected = true;
                $('#alert').html('');
              }
              else {
                $('.'+item+'').attr('src', 'static/img/icon_'+item+'.png');
                $('.'+item+'Input').prop('checked', false);
                selected = false;
              }
            }
            function validate() {
              if(selected) {
                $('form').submit();
              }
              else {
                $("#alert").html('U heeft nog geen kledingtype aangeklikt');
              }
            }
          </script>

          <input class="imgCheckbox shirtInput" type="checkbox" name="shirt"/>
          <input class="imgCheckbox truiInput" type="checkbox" name="trui"/>
          <input class="imgCheckbox broekInput" type="checkbox" name="broek"/>
          <input class="imgCheckbox jasInput" type="checkbox" name="jas"/>
          <input class="imgCheckbox hoofddekselInput" type="checkbox" name="hoofddeksel"/>
          <input class="imgCheckbox overhemdInput" type="checkbox" name="overhemd"/>
          <input class="imgCheckbox schoenenInput" type="checkbox" name="schoenen"/>

          <p>Geef hieronder minimaal 1 kledingstuk aan</p>
          <div class="inlineBlock">
            <div class="imgDiv">
              <img class="img shirt" src="static/img/icon_shirt.png" onclick="select( 'shirt' )" alt="Shirt" title="Shirt"/>
              <img class="img trui" src="static/img/icon_trui.png" onclick="select( 'trui' )" alt="Trui" title="Trui"/>
              <img class="img broek" src="static/img/icon_broek.png" onclick="select( 'broek' )" alt="Broek" title="Broek"/>
              <img class="img jas" src="static/img/icon_jas.png" onclick="select( 'jas' )" alt="Jas" title="Jas"/>
            </div>
            <div class="imgDiv">
              <img class="img hoofddeksel" src="static/img/icon_hoofddeksel.png" onclick="select( 'hoofddeksel' )" alt="Hoofddeksel" title="Hoofddeksel"/>
              <img class="img overhemd" src="static/img/icon_overhemd.png" onclick="select( 'overhemd' )" alt="Overhemd" title="Overhemd"/>
              <img class="img schoenen" src="static/img/icon_schoenen.png" onclick="select( 'schoenen' )" alt="Schoenen" title="Schoenen"/>
            </div>
            <div style="clear:both"></div>
          </div>
          <div id="alert" style="height:30px"></div>

          <table class="footer">
            <tr>
              <td><input type="submit" class="submit btn" formaction="addItem.php" value="Meer toevoegen" /></td>
              <td><a class="submit btn match" onclick="validate()" >Match outfit</a></td>
            </tr>
          </table>
        </form>
      </div>
    </div>
  </body>
</html>