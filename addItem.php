<?php
  session_start();
  require_once( "objects/Item.php");
  require_once( "objects/Clothing.php");
  require_once( "connect.php");
?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="static/css/general.css" />
    <link rel="stylesheet" type="text/css" href="static/css/addItem.css" />
    
    <script src="static/libs/jquery-2.1.4.js" ></script>
    <script src="static/libs/jquery.validate.js" ></script>
    
    <link rel='stylesheet' href='static/libs/spectrum/spectrum.css' />
    <script src='static/libs/spectrum/spectrum.js'></script>
    
    <title>Add Item</title>
  </head>
  
  <body>
    <div id="mainContainer">
      <div id='containerCenter' style="position: relative">
        <img src="static/img/beeldmerk_MuStLG.png" class="logo" />
        <a href="login.php" class="logout btn">Log uit</a>
        <h1>Upload een item</h1>
        <h6><b class="requiredStar">*</b> verplicht</h6>
        <form id="addItem" method="post" enctype="multipart/form-data" action="itemAdded.php">
          <script>
            var selected = false;
            function select(item) {
              $('img').each( function () {
                var img = $(this).attr('src');
                var string = img.replace("_selected", "");
                $(this).attr('src', string);
              });
              if($('.'+item+'').attr('src') === 'static/img/icon_'+item+'.png') {
                $('.'+item+'').attr('src', 'static/img/icon_'+item+'_selected.png');
                $('.'+item+'Input').prop('checked', true);
                selected = true;
                $('#alert').hide();
              }
              else {
                $('.'+item+'').attr('src', 'static/img/icon_'+item+'.png');
                $('.'+item+'Input').prop('checked', false);
                selected = false;
              }
            }
            function validate(buttonType) {
              if(selected) {
                if($('form').validate()) {
                  if(buttonType == 'addMore') {
                    $('.addMore').click();
                  }
                  else {
                    $('.generate').click();
                  }
                }
              }
              else {
                $("#alert").html('U heeft nog geen kledingtype aangeklikt').show();
              }
            }
          </script>
          
          <input class="imgCheckbox shirtInput" type="checkbox" name="shirt"/>
          <input class="imgCheckbox truiInput" type="checkbox" name="trui"/>
          <input class="imgCheckbox broekInput" type="checkbox" name="broek"/>
          <input class="imgCheckbox jasInput" type="checkbox" name="jas"/>
          <input class="imgCheckbox hoofddekselInput" type="checkbox" name="hoofddeksel"/>
          <input class="imgCheckbox overhemdInput" type="checkbox" name="overhemd"/>
          <input class="imgCheckbox schoenenInput" type="checkbox" name="schoenen"/>
          
          <div class="inlineBlock">
            <div class="imgDiv">
              <img class="img shirt" src="static/img/icon_shirt.png" onclick="select('shirt')" alt="Shirt" title="Shirt"/>
              <img class="img trui" src="static/img/icon_trui.png" onclick="select('trui')" alt="Trui" title="Trui"/>
              <img class="img broek" src="static/img/icon_broek.png" onclick="select('broek')" alt="Broek" title="Broek"/>
              <img class="img jas" src="static/img/icon_jas.png" onclick="select('jas')" alt="Jas" title="Jas"/>
            </div>
            <div class="imgDiv">
              <img class="img hoofddeksel" src="static/img/icon_hoofddeksel.png" onclick="select('hoofddeksel')" alt="Hoofddeksel" title="Hoofddeksel"/>
              <img class="img overhemd" src="static/img/icon_overhemd.png" onclick="select('overhemd')" alt="Overhemd" title="Overhemd"/>
              <img class="img schoenen" src="static/img/icon_schoenen.png" onclick="select('schoenen')" alt="Schoenen" title="Schoenen"/>
            </div>
          </div>
          <div style="clear:both"></div>
          <div id="alert"></div>
          
          <table class="addItemTable">
            <tr>
              <td>
                <div class="boxName">Kleur *</div>
                <div class="select">
                  <input style="width: 172px" id="showPaletteOnly" type="text" name="color" value="green" readonly required >
                </div>
              </td>

              <td>
                <div class="boxName">Aankoopdatum</div>
                <input class="shortInputBox" type="text" name="dateOfPurchase" placeholder="dd-mm-yyyy" pattern="[0-3]{1}[0-9]{1}-[0|1]{1}[0-9]{1}-[1|2]{1}[0|9]{1}[0-9]{2}" title="dd-mm-yyyy" >
              </td>
            </tr>
            
            <tr>
              <td>
                <div class="boxName">Style *</div>
                <div class="select">
                  <select style="width: 172px" class="select" name="style" required >
                    <option></option>
                    <option value="Arty">Arty</option>
                    <option value="Chic">Chic</option>
                    <option value="Classic">Classic</option>
                    <option value="Casual">Casual</option>
                    <option value="Exotic">Exotic</option>
                    <option value="Sophisticated">Sophisticated</option>
                    <option value="Western">Western</option>
                    <option value="Traditional">Traditional</option>
                    <option value="Punk">Punk</option>
                    <option value="Rocker">Rocker</option>
                    <option value="Gothic">Gothic</option>
                  </select>
                </div>
              </td>
              
              <td>
                <div class="boxName">Maat</div>
                <div class="select">
                  <select style="width: 172px" class="select" name="size">
                    <option value="XS">XS</option>
                    <option value="S">S</option>
                    <option value="M">M</option>
                    <option value="L">L</option>
                    <option value="XL">XL</option>
                    <option value="XXL">XXL</option>
                  </select>
                </div>
              </td>
            </tr>
            
            <tr>
              <td>
                <div class="boxName">Seizoen *</div>
                <div class="select">
                  <select style="width: 172px" class="select" name="season" required >
                    <option></option>
                    <option  value="all">Alle seizoenen</option>
                    <option  value="zomer">Zomer</option>
                    <option  value="herfst">Herfst</option>
                    <option  value="winter">Winter</option>
                    <option  value="lente">Lente</option>
                  </select>
                </div>
              </td>
              
              <td>
                <div class="boxName">Prijs</div>
                <input class="shortInputBox" type="integer" name="price" placeholder="19,95">
              </td>
            </tr>
            
          <script>
            $().ready( function () {
              $("#form").validate({
                rules: {
                  brand: {
                    required: true
                  }
                },
                messages: {
                  brand: "Required Field"
                }
              });
              
              $('#alert').hide();
              
              $("#showPaletteOnly").spectrum({
                preferredFormat: "name",
                hideAfterPaletteSelect:true,
                showPaletteOnly: true,
                showPalette: true,
                allowEmpty: false,
                color: "green",
                palette: [
                  ['black', 'navy', 'maroon', 'purple', 'olive'],
                  ['grey', 'blue', 'brown', 'fuchsia', 'green'],
                  ['silver', 'teal', 'orange', 'violet', 'lime'],
                  ['white', 'aqua', 'red', 'pink', 'yellow']
                ]
              });
              $(".browseBtn").click( function () { //Bij het klikken op de browseBtn, wordt er hiermee op de input-type=file-btn geklikt
                $("#uploadInput").click();
              });
              $("#uploadInput").change( function () { //Als er een bestand is gekozen, wordt de naam in een andere textbox weergegeven
                $("#fileNameBox").val(this.files[0].name);
              });
              $('.clearBtn').click( function () { //Wis de tekst in de textbox
                $('#fileNameBox').val("");
                $('#uploadInput').val("");
              });
              // IK PROBEER FOCUS OP DE KLEURENBOX TE KRIJGEN VIA DE TAB-TOETS MAAR HET LUKT NIET!
              //$(".sp-replacer").attr("tabindex","4").focus();
            });
            /*$('body').live('keydown', function(e) {
              if (is_dialog_visible) {
                var keyCode = e.keyCode || e.which;
                if (keyCode === 2) {
                  e.preventDefault();
                  if (container.find('[name="subtype"]').is(":focus")) {
                    container.find(".sp-replacer").focus();
                  } else {
                    container.find(".sp-replacer").focus();
                  }
                }
              }
            });
            /*$('body').live("keydown", function(e) {
                if($("[name='subtype']").is(":focus"))
                {
                  if (is_dialog_visible) {
                    var keyCode = e.keyCode || e.which;
                    if (keyCode === 27) {
                      e.preventDefault();
                      $("sp-replacer").focus();
                    }
                  }
                }
              });*/
          </script>
          <tr>
            <td>
              <div class="fileUploadDiv">
                <div class="boxName">Bon uploaden</div>
                <input id="fileNameBox" type="text" placeholder="Geen bestand gekozen" disabled >
                <input id="uploadInput" type="file" name="receipt" accept=".png,.jpg,.jpeg" readonly >
              </div>
            </td>
            <td>
              <input class="browseBtn btn" type="button" value="Bladeren">
              <input class="clearBtn btn" type="button" value="Wis">
            </td>
          </tr>
          </table>
          
          <div class="textBoxes">
            <div class="boxName">Merk *</div>
            <input class="input" type="text" name="brand" placeholder="Adidas / Nike / Armani" required />
          </div>
          
          <div class="textBoxes">
            <div class="boxName">Subtype</div>
            <input class="input" type="text" name="subtype" placeholder="Korte broek / Baseballcap / Hoed">
          </div>
          
          <div class="textBoxes">
            <div class="boxName">Materiaal</div>
            <input class="input" type="text" name="material" placeholder="Katoen / Wol / Polyester">
          </div>
          
          <div class="textBoxes">
            <div class="boxName">Omschrijving</div>
            <input class="input" type="text" name="description" placeholder="Favoriet / Lekker warm / Cadeau">
          </div>
          
          <div class="available"><input type="checkbox" checked name="available" value=1>Momenteel beschikbaar</div>
          
          <input class="generate" type="submit" name="generate" hidden />
          <input class="addMore" type="submit" name="addMore" hidden />
          
          <table class="footerTable">
            <tr>
              <td><input type="button" class="btn" onclick="validate('addMore')" value="Meer toevoegen"></td>
              <td><input type="button" id="generate" class="btn" onclick="validate('generate')" value="Match kleding"></td>
            </tr>
          </table>
        </form>
      </div>
    </div>
  </body>
</html>
