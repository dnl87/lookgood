<?php
namespace must;

class Item {
  //props
  private $itemId  = 0;
  
  protected
    $brand          = "Geen invoer",
    $type           = "Geen invoer",
    $subtype        = "Geen invoer",
    $color          = "Geen invoer",
    $style          = "Geen invoer",
    $material       = "Geen invoer",
    $dateOfPurchase = "Geen invoer",
    $receipt        = "blob",
    $description    = "Geen invoer",
    $available      = 0,
    $price          = 0.0,
    $size           = "M";
    
  //functions
  function __construct( $conn ) {
    $this->conn = $conn;
  }
  
  public function insert( $propertiesArray ) {
    $this->type = addslashes( $propertiesArray['type'] );
    $this->style = addslashes( $propertiesArray['style'] );
    $this->brand = addslashes( $propertiesArray['brand'] );
    $this->color = $propertiesArray['color'];
    
    $this->available = $propertiesArray['available'] ? 1 : 0;
    
    if( isset( $propertiesArray['subtype'] ) ) {
      self::setSubtype( $propertiesArray['subtype'] );
    }
    if( isset( $propertiesArray['material'] ) ) {
      self::setMaterial( $propertiesArray['material'] );
    }
    if( isset( $propertiesArray['dateOfPurchase'] ) ) {
      self::setDateOfPurchase( $propertiesArray['dateOfPurchase'] );
    }
    if( isset( $propertiesArray['description'] ) ) {
      self::setDescription( $propertiesArray['description'] );
    }
    if( isset( $propertiesArray['price'] ) ) {
      self::setPrice( $propertiesArray['price'] );
    }
    if( isset( $propertiesArray['size'] ) ) {
      self::setSize( $propertiesArray['size'] );
    }
    
    $sql = "INSERT INTO `must`.`item` ( `type`, `color`, `style`, `brand`, `subtype`, `material`, `dateOfPurchase`, `description`, `price`, `available`, `size` )
            VALUES ( '$this->type', '$this->color', '$this->style', '$this->brand', '$this->subtype', '$this->material', '$this->dateOfPurchase', '$this->description', '$this->price', '$this->available', '$this->size' )";
    
    $result = $this->conn->query( $sql );
    $this->itemId = $this->conn->insert_id;
    
    if( $result ) {
      return $this->itemId;
    }
    else {
      throw new Exception( "Uw upload is helaas niet gelukt door een technische storing.<br>"
              . "Klik <a href='addItem.php' title='item toevoegen'>hier</a> om opnieuw te proberen." );
    }
  }
  
  function getItemOnProperty( $propertyName, $property ) {
    $sql = "SELECT brand, subtype, color, description, size FROM item WHERE $propertyName = '$property'";
    
    $result = $this->conn->query( $sql );
    
    $properties = [];
    
    if( $result->num_rows > 0 ) {
      while( $row = $result->fetch_assoc() ) {
        $properties["brand"] = $row['brand'];
        $properties["subtype"] = $row['subtype'];
        $properties["color"] = $row['color'];
        $properties["description"] = $row['description'];
        $properties["size"] = $row['size'];
      }
    }
    return $properties;
  }
  
  //getters
  public function getItem_Id() {
    return $this->item_ID;
  }

  public function getBrand() {
    return $this->brand;
  }

  public function getType() {
    return $this->type;
  }

  public function getSubtype() {
    return $this->subtype;
  }

  public function getColor() {
    return $this->color;
  }

  public function getStyle() {
    return $this->style;
  }

  public function getMaterial() {
    return $this->material;
  }

  public function getDateOfPerchase() {
    return $this->dateOfPerchase;
  }

  public function getReceipt() {
    return $this->receipt;
  }

  public function getDescription() {
    return $this->description;
  }

  public function getAvailable() {
    return $this->available;
  }

  public function getPrice() {
    return $this->price;
  }

  public function getSize() {
    return $this->size;
  }

  //setters
  private function setItem_ID( $item_ID ) {
    $this->item_ID = $item_ID;
  }
  
  public function setBrand( $brand ) {
    $this->brand = $brand;
  }

  public function setType( $type ) {
    $this->type = $type;
  }

  public function setSubtype( $subtype ) {
    $this->subtype = $subtype;
  }

  public function setColor( $color ) {
    $this->color = $color;
  }

  public function setStyle( $style ) {
    $this->style = $style;
  }

  public function setMaterial( $material ) {
    $this->material = addslashes( $material );
  }

  public function setDateOfPurchase( $dateOfPurchase ) {
    $dateOfPurchase = preg_replace( '#(\d{2})-(\d{2})-(\d{4})#', '$3-$2-$1', $dateOfPurchase );
    $this->dateOfPurchase = $dateOfPurchase;
  }

  public function setReceipt( $receipt ) {
    $this->receipt = $receipt;
  }

  public function setDescription( $description ) {
    $this->description = $description;
  }

  public function setAvailable( $available ) {
    $this->available = $available;
  }

  public function setPrice( $price ) {
    $this->price = $price;
  }

  public function setSize( $size ) {
    $this->size = $size;
  }
}