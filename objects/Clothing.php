<?php
namespace must;

class Clothing extends Item
{
  private $clothing_ID = 0;
  
  protected
    $pattern   	    = "NONE",
    $season         = "Geen invoer";
  
  function __construct( $conn )
  {
    Item::__construct( $conn );
  }
  
  //setters
  function setPattern( $pattern ) {
    $this->pattern = $pattern;
  }

  function setSeason( $season ) {
    $this->season = $season;
  }
  
  //other functions
  function insertClothing( $propertiesArray ) {
    $itemId = parent::insert( $propertiesArray );
    
    $this->season = $propertiesArray['season'];
    $userId = $propertiesArray['userId'];

    $sql = "INSERT INTO `clothing`( `item_ID`, `pattern`, `season`, `user_ID` )
            VALUES ( '$itemId', '$this->pattern', '$this->season', '$userId' )";
    
    $result = $this->conn->query( $sql );

    if( $result ) {
      return true;
    }
    else {
      throw new \Exception( "Het uploaden is helaas niet gelukt door een technische storing."
              . "Klik <a href='addItem.php' title='Inloggen of registreren'>hier</a> om opnieuw te proberen." );
    }
  }
  
  function concatSqlQueryWithANDcomparison( $propertyName, $property ) {
    $concatQuery = isset( $property ) && $property ? "AND $propertyName = '$property'" : "";
    
    return $concatQuery;
  }
  
  function sqlSelectQueryOnConditions( $userId, $type, $style, $brand, $season ) {
    $brandComparisonConcatQuery = self::concatSqlQueryWithANDcomparison('brand', $brand);
    
    $sql = "SELECT clothing.item_ID, pattern FROM clothing "
                  . "INNER JOIN item ON item.item_ID = clothing.item_ID "
                  . "WHERE user_ID = '$userId' AND style = '$style' AND type = '$type' "
                  . "AND available = 1 AND (season = '$season' OR season = 'all')".$brandComparisonConcatQuery;
    $result = $this->conn->query( $sql );
    
    return $result;
  }
  
  public function getRandomIndexOfArray( $array ) {
    $amount = count( $array );
    
    $randomIndexOfArray = mt_rand( 1, $amount );
    
    return $randomIndexOfArray;
  }
  
  public function getOneItemOnConditions( $userId, $type, $style, $brand, $season ) {
    $properties = [];
    
    $result = self::sqlSelectQueryOnConditions( $userId, $type, $style, $brand, $season );
    
    if( $result->num_rows > 0 ) {
      $i = 1;
      while( $row = $result->fetch_assoc() ) {
        $itemIdArray[$i] = $row['item_ID'];
        $patternArray[$i] = $row['pattern'];
        $i++;
      }
      
      $randomIndexOfItemsArray = self::getRandomIndexOfArray( $itemIdArray );
      
      $itemId = $itemIdArray[$randomIndexOfItemsArray];
      // random = bv 5 en 5 wordt dan voor alle arrays gebruikt
      // Hierboven in de while-loop worden de arrays gelijktijdig op dezelfde plaats gestopt
      $properties["pattern"] = $patternArray[$randomIndexOfItemsArray];
      
      $result2 = parent::getItemOnProperty( 'item_ID', $itemId );
      
      foreach( $result2 as $key => $value ) {
        $properties[$key] = $value;
      }
    }
    else {
      throw new \Exception("Sorry, er is iets fout gegaan.");
    }
    return $properties;
  }
  
  public function sqlSelectQueryWithOneColumnGrouped( $userId, $columnName ) {
    $sql = "SELECT $columnName FROM item INNER JOIN clothing ON item.item_ID = clothing.item_ID WHERE user_ID = '$userId' GROUP BY $columnName";
    
    $result = $this->conn->query( $sql );
    
    return $result;
  }
  
  public function createSelectOptions( $userId, $columnName ) {
    $result = self::sqlSelectQueryWithOneColumnGrouped( $userId, $columnName );
    
    // kijken of er resultaten zijn
    if( $result->num_rows > 0 ) {
      $counter = 0;
      while( $row = $result->fetch_assoc() ) {
        $columnName = $row[$columnName];
        $htmlOptionsArray[$counter] = '<option value="'.$columnName.'">'.ucfirst( strtolower( $columnName ) ).'</option>';
        $counter++;
      }
      return $htmlOptionsArray;
    }
  }
}