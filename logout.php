<?php
  session_start();
  require_once 'connect.php';
  require_once 'objects\User.php';

  $user = new must\User( $conn );
  $user->logout();
  header( "Location: index.php" );