<?php
  session_start();

  require_once( "connect.php" );
  require_once( "objects/User.php" );

  $user = new \must\User( $conn );

  $login = filter_input( INPUT_POST, 'login' );
  $email = filter_input( INPUT_POST, 'email' );
  $password = filter_input( INPUT_POST, 'passwordLogin' );

  if( isset( $login ) &&  !$user->checkPassword( $email, $password ) )
  {
    $_SESSION["email"] = $email;
    if ( $user->retrieveIdByMail( $email ) > 99999 )
    {
      $_SESSION["wrongMail"] = false;
    }
    else
    {
      $_SESSION["wrongMail"] = true;
    }
    header( "Location: login.php" );
  }
  else
  {
    $_SESSION["userId"] = $user->retrieveIdByMail( $email );
    header( "Location: preference.php" );
  }