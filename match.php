<!DOCTYPE html>
<?php
  session_start();
  require_once 'connect.php';
  require_once 'objects\Item.php';
  require_once 'objects\Clothing.php';
  require_once 'objects\User.php';
?>
<html>
  <head>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="static/css/general.css" />
    <link rel="stylesheet" type="text/css" href="static/css/match.css" />
    
    <script src="static/libs/jquery-2.1.4.js" ></script>
    
    <link rel='stylesheet' href='static/libs/spectrum/spectrum.css' />
    <script src='static/libs/spectrum/spectrum.js'></script>
    
    <title>Matched</title>
  </head>
  <body>
    <div id="mainContainer">
      <div id='containerCenter'>
        <img src="static/img/beeldmerk_MuStLG.png" class="logo" />
        <a href="login.php" class="logout btn">Log uit</a>
        
        <h1>Match</h1>
        <div style="clear:both"></div>
        <table class="innerMatchTable">
<?php
          $userId = $_SESSION['userId'];
          
          $type = [];
          
          $style = filter_input( INPUT_POST, "style" );
          $brand = filter_input( INPUT_POST, "brand" );
          $season = filter_input( INPUT_POST, "season" );
          $shirt = filter_input( INPUT_POST, "shirt" );
          $trui = filter_input( INPUT_POST, "trui" );
          $broek = filter_input( INPUT_POST, "broek" );
          $hoofddeksel = filter_input( INPUT_POST, "hoofddeksel" );
          $overhemd = filter_input( INPUT_POST, "overhemd" );
          $schoenen = filter_input( INPUT_POST, "schoenen" );
          $jas = filter_input( INPUT_POST, "jas" );
          
          $clothing = new must\Clothing( $conn );
          
          $type['shirt'] = isset( $shirt ) ? true : false;
          $type['trui'] = isset( $trui ) ? true : false;
          $type['broek'] = isset( $broek ) ? true : false;
          $type['hoofddeksel'] = isset( $hoofddeksel ) ? true : false;
          $type['overhemd'] = isset( $overhemd ) ? true : false;
          $type['schoenen'] = isset( $schoenen ) ? true : false;
          $type['jas'] = isset( $jas ) ? true : false;
          
          try {
            if( isset( $style ) ) {
              foreach( $type as $typeKey => $value ) {
                if( $value ) {
                  $clothingItemArray = $clothing->getOneItemOnConditions( $userId, $typeKey, $style, $brand, $season );
                  if( !empty( $clothingItemArray ) ) {
                    $brandView = $clothingItemArray['brand'];
                    $sizeView = $clothingItemArray['size'];
                    $colorView = $clothingItemArray['color'];
                    $subtypeView = $clothingItemArray['subtype'];
                    $patternView = $clothingItemArray['pattern'];
                    $descriptionView = $clothingItemArray['description'];
                    
                    $view = "<tr>"
                            . "<td>"
                              . "<img class='imgMatch' src='static/img/icon_".$typeKey.".png' style=''/>"
                            . "</td>"
                            
                            . "<td>"
                              . "<div class='matchPropertiesDiv matchOne'>"
                                . "<b>Merk: </b>" . ucfirst( strtolower( $brandView ) ) . "<br>"
                                . "<b>Maat: </b>" . ucfirst( strtolower( $sizeView ) ) . "<br>"
                                . "<b>Kleur: </b>" . ucfirst( strtolower( $colorView ) ) . ""
                              . "</div>"
                              
                              . "<div class='matchPropertiesDiv matchTwo'>"
                                . "<b>Subtype: </b>" . ucfirst( strtolower( $subtypeView ) ) . "<br>"
                                . "<b>Patroon: </b>" . ucfirst( strtolower( $patternView ) ) . "<br>"
                                . "<b>Omschrijving: </b>" . ucfirst( strtolower( $descriptionView ) )
                              . "</div>"
                            . "</td>"
                          . "</tr>";
                    echo $view;
                  }
                }
              }
            }
          }
          catch( Exception $e ) {
            $exception = $e->getMessage();
            echo "<p>" . $exception . " Ga <a href='preference.php' title='Preferences'>terug</a> en probeer het opnieuw.</p>";
          }
?>
        </table>
        <script>
          function reload() {
            location.reload(true);
          }
        </script>
        <form method='POST'>
          <input type="hidden" name="brand" value="<?php echo $brand ?>"/>
          <input type="hidden" name="style" value="<?php echo $style ?>"/>
          <input type="hidden" name="season" value="<?php echo $season ?>"/>
          
          <input class="imgCheckbox" type="checkbox" name="shirt" <?php echo isset( $shirt ) ? 'checked' : '' ?> />
          <input class="imgCheckbox" type="checkbox" name="trui" <?php echo isset( $trui ) ? 'checked' : '' ?> />
          <input class="imgCheckbox" type="checkbox" name="broek" <?php echo isset( $broek ) ? 'checked' : '' ?> />
          <input class="imgCheckbox" type="checkbox" name="jas" <?php echo isset( $jas ) ? 'checked' : '' ?> />
          <input class="imgCheckbox" type="checkbox" name="hoofddeksel" <?php echo isset( $hoofddeksel ) ? 'checked' : '' ?> />
          <input class="imgCheckbox" type="checkbox" name="overhemd" <?php echo isset( $overhemd ) ? 'checked' : '' ?> />
          <input class="imgCheckbox" type="checkbox" name="schoenen" <?php echo isset( $schoenen ) ? 'checked' : '' ?> />
          <table class="footer">
            <tr>
              <td><button class="btn" formaction="preference.php">Preferences</button></td>
              <td><button class="btn" formaction="match.php">Opnieuw</button></td>
            </tr>
          </table>
        </form>
      </div>
    </div>
  </body>
</html>